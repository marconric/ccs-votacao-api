# ccs-votacao-api

##Descrição

API será responsável por controlar votação de assembleia 
dos associados da cooperativa, serão criado novas pautas, 
criação de sessão para realizar votação por parte dos associados para aprovar ou reprovar as mesmas, alem de computar o resultado enviando resultado por Mensageria.
Este exercício tratando-se de uma maneira de verificação dos conhecimento técticos e boas práticas no desenvolvimento de Back-End

---

##Tecnologias envolvidas
Java 8/ Kotlin / Hibernate  / Spring /  H2  / GIT / Kafka
---

##Processo para rodar API
 Verificar se o servidor possui banco H2 rodando se caso não possuir segue link para realização da configuração do mesmo
[Instalação H2](https://o7planning.org/11895/install-h2-database-and-use-h2-console)

####Configurações a serem realizadas (application)

Após realizar a configuração do banco no passo anterior e criar diretório para armazenar

**Verificar as configurações da base que foi configurado na base H2 para ajustar na API**

> url: Caminho Base   EX. (jdbc:h2:/home/user/db/base;DB_CLOSE_ON_EXIT=FALSE;AUTO_RECONNECT=TRUE)
>
> username: Usuário
>
> password: Senha

Neste existem 3 arquivos de configuração YML
**application.yml** - arquivo do projeto aonde consta o profile que será usado para configurar o ambiente

**application-dev.yml** - Apontamento para utilização em ambiente de desenvolvimento

**appication-container.yml** Apontamento para utilização em ambiente de produção

[Aplication YML](src/main/resources/application.yml)

####Configurações a serem realizadas
> CCS_PORTA  Porta que vai rodar a aplicação está setado a 13000 como default
>
> CCS_BASE Cuidado com esta configuração para rodar a primeira vez e criar os objetos da base nesta antes de rodar a aplicação e com a base H2 já configurada setar (create) após rodar a primeira vez alterar para (update ou none)   
>
> CCS_CONSULTA_CPF_URL URL para consumir API de validação de CPF
>
> CCS_SCHEDULER   Scheduler para realizar o controle de encerramento da sessão

 ---

####Fluxo para utilização processamento
```mermaid
graph TD
    AA((Início))
    -->BB(Cria Pauta)
    -->CC[Pauta Salva]
    AA-->DD(Cria Sessão)
    -->EE{Existe Pauta}
    EE -->|Sim| FF[Salva Sessão]
    EE -->|Não| GG>Retorna]
    AA-->HH(Associado Votar)
    -->II{Existe Sessão Aberta}
    II -->|Sim| JJ[Salva Sessão]
    II -->|Não| LL>Retorna]
    JJ --> LL{Verifica Associado}
    LL -->|ABLE_TO_VOTE| MM[Salva Voto]
    LL -->|UNABLE_TO_VOTE| NN>Retor]
```


