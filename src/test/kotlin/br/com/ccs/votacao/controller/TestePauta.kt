import br.com.ccs.votacao.model.enum.SimNaoEnum
import br.com.ccs.votacao.model.request.PautaRequest
import br.com.ccs.votacao.model.response.ApiResponse
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.net.URI
import java.util.*


@SpringBootTest
@DataJpaTest
class TestePauta {
    private val porta = "15000"
    private val versaoApi = "v1"
    private val urlBase = "http://localhost:$porta/api/$versaoApi/"

    @Autowired
    private val testTemplate = TestRestTemplate()


    @Test
    fun insertPauta() {

        val urlPautaInsert = "${urlBase}incluirPauta"
        val uri = URI(urlPautaInsert)
        val pautaTeste = PautaRequest("Pauta avaliação candidato", "Avaliar o Candidato para verificar suas  compentências")

        val headers = HttpHeaders()

        val request: HttpEntity<PautaRequest> = HttpEntity(pautaTeste, headers)

        val result: ResponseEntity<ApiResponse<*>>? = this.testTemplate.postForEntity(uri, request, ApiResponse::class.java)
        assertNotNull(result)
        assertEquals(result?.statusCode, HttpStatus.OK)
    }

    @Test
    fun findPauta() {
        val urlPauta = "${urlBase}findAllPautas"
        val response = testTemplate.getForEntity("$urlPauta", ApiResponse::class.java)
        assertEquals(response.statusCode, HttpStatus.OK)
    }


    @Test
    fun iniciarSessao() {
        val urlSessaoInsert = "${urlBase}iniciarSessao?idPauta=1&tempoSessao=20"

        val uri = URI(urlSessaoInsert)

        val headers = HttpHeaders()

        val request: HttpEntity<*> = HttpEntity("", headers)

        val result: ResponseEntity<ApiResponse<*>>? = this.testTemplate.postForEntity(uri, request, ApiResponse::class.java)
        assertNotNull(result)
        assertEquals(result?.statusCode, HttpStatus.OK)
    }

    @Test
    fun geraVoto() {
        val listCpfs = Arrays.asList("525.494.117-06",
                "195.458.221-80",
                "681.267.639-06",
                "027.037.676-33",
                "627.319.114-09",
                "706.200.694-73",
                "944.425.391-03",
                "632.597.820-01",
                "306.582.864-20",
                "032.566.498-66",
                "111.906.585-26",
                "494.502.528-23",
                "329.588.387-49",
                "523.594.466-65",
                "173.026.548-08",
                "541.846.818-91",
                "061.479.261-44",
                "879.611.936-50",
                "153.312.323-37",
                "846.295.772-97",
                "950.429.978-49",
                "023.385.048-10",
                "561.702.249-08",
                "585.491.141-89",
                "293.534.300-03",
                "716.700.273-92",
                "371.722.509-07",
                "684.289.913-32",
                "300.637.947-05",
                "542.866.461-49",
                "693.420.666-58",
                "945.976.169-00",
                "737.943.586-86",
                "363.272.603-57",
                "942.998.178-18",
                "153.866.182-96",
                "737.412.459-76",
                "177.164.645-40")



        if (listCpfs != null && !listCpfs.isEmpty()) {

            var voto: SimNaoEnum = SimNaoEnum.NAO
            for (i in listCpfs) {

                voto = if (voto.equals(SimNaoEnum.SIM)) {
                    SimNaoEnum.SIM
                } else {
                    SimNaoEnum.NAO
                }
                val urlSessaoInsert = "${urlBase}votar?cpf=${i}&idSessao=1&voto=${voto}"
                val uri = URI(urlSessaoInsert)

                val headers = HttpHeaders()

                val request: HttpEntity<*> = HttpEntity("", headers)

                val result: ResponseEntity<ApiResponse<*>>? = this.testTemplate.postForEntity(uri, request, ApiResponse::class.java)
                assertNotNull(result)
                assertEquals(result?.statusCode, HttpStatus.OK)
            }
        }

    }

}