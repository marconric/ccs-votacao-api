package br.com.ccs.votacao.controller

import br.com.ccs.votacao.model.request.PautaRequest
import br.com.ccs.votacao.model.response.ApiResponse
import br.com.ccs.votacao.service.PautaService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1")
@Api(description = "Manipular as informações das Pautas", tags = ["Pauta Assembleia"])
class PautaController(private val pautaService: PautaService) {

    private var log = LoggerFactory.getLogger(this::class.java)

    @PostMapping("/incluirPauta")
    @ApiOperation("Processo para  efetuar inclusão de uma nova Pauta")
    fun incluirPauta(@RequestBody pautaRequest: PautaRequest): ApiResponse<Any> {
        log.info("Inicio chamada inclusão Pauta")

        val retorno = pautaService.insertPauta(pautaRequest)

        return ApiResponse(retorno)
    }

    @GetMapping("/findAllPautas")
    @ApiOperation("Processo para efetua busca de todas as Pautas")
    fun findAllPautas(): ApiResponse<Any?> {
        log.info("Busta todas as Pautas")
        val list = pautaService.findAllPautas()
        return if (list.isNullOrEmpty()) {
            ApiResponse("Não possuem pautas.")
        } else {
            ApiResponse(list)
        }

    }

    @GetMapping("/findPautaById")
    @ApiOperation("Processo para efetua busca Pauta por ID.")
    fun findPautaById(@RequestParam id: Long): ApiResponse<Any?> {
        log.info("Busca  pauta por id $id")
        return ApiResponse(pautaService.findPautaById(id))
    }

    @DeleteMapping("/excluirPauta/{id}")
    @ApiOperation("Processo para efetua exclusão Pauta por ID.")
    fun deletePauta(@PathVariable id: Long): ApiResponse<Any?> {

        log.info("Chamado processo de esclusão pauta com  $id")
        return ApiResponse(pautaService.excluirPauta(id))
    }


}