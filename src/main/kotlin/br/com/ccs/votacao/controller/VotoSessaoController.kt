package br.com.ccs.votacao.controller

import br.com.ccs.votacao.model.enum.SimNaoEnum
import br.com.ccs.votacao.model.response.ApiResponse
import br.com.ccs.votacao.service.VotoSessaoService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1")
@Api(description = "Manipular as informações dos votos", tags = ["Voto Sessão"])
class VotoSessaoController(private val votoSessaoService: VotoSessaoService) {

    private var log = LoggerFactory.getLogger(this::class.java)

    @PostMapping("/votar")
    @ApiOperation("Iniciar Voto Pauta referente Sessão")
    fun votar(
            @RequestParam idSessao: Long,
            @RequestParam cpf: String,
            @RequestParam voto: SimNaoEnum
    ): ApiResponse<Any> {
        log.info("Inicio chamada inclusão Pauta")

        return ApiResponse(votoSessaoService.realizarVoto(idSessao, cpf, voto))
    }

}