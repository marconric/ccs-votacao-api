package br.com.ccs.votacao.controller

import br.com.ccs.votacao.model.response.ApiResponse
import br.com.ccs.votacao.service.SessaoService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1")
@Api(description = "Manipular as informações das Sessões", tags = ["Sessão"])
class SessaoController(private val sessaoService: SessaoService) {

    private var log = LoggerFactory.getLogger(this::class.java)

    @PostMapping("/iniciarSessao")
    @ApiOperation("Iniciar sessão de votação da Pauta")
    fun iniciarSessao(@RequestParam idPauta: Long,
                      @RequestParam tempoSessao: Int? = 1): ApiResponse<Any> {
        log.info("Inicio chamada inclusão sessão")
        var tempo: Int = 1
        if (tempoSessao != null && tempoSessao != 0) {
            tempo = tempoSessao
        }

        return ApiResponse(sessaoService.iniciarSessao(idPauta, tempo))
    }

    @PutMapping("/prorrogaSessao")
    @ApiOperation("Processo para realizar prorrogação da sessão")
    fun prorrogaSessao(@RequestParam idSessao: Long,
                       @RequestParam tempoAcrecido: Int? = 1): ApiResponse<Any> {

        return ApiResponse(sessaoService.prorrogaSessao(idSessao, tempoAcrecido!!))
    }


    @GetMapping("/buscaSessaoById/{id}")
    @ApiOperation("Processo que efetua busca Sessão por ID.")
    fun buscaSessaoById(@RequestParam id: Long): ApiResponse<Any?> {
        log.info("Busca  sessão por id $id")
        return ApiResponse(sessaoService.buscaSessaoById(id))
    }


}