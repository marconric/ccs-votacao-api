package br.com.ccs.votacao.utils

import org.springframework.http.HttpStatus

data class AppException(
        override val message: String,
        var status: HttpStatus?= null
) : Exception() {
    override fun toString(): String {
        return message
    }
}