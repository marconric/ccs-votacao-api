package br.com.ccs.votacao.utils

import java.text.Normalizer
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class Utils {
    companion object {
        fun dataFormatada(data: Date, format: String? = "dd/MM/yyyy"): String {
            return SimpleDateFormat(format).format(data)
        }

        fun dataFormatada(data: LocalDateTime, format: String? = "dd/MM/yyyy HH:mm:ss"): String {
            return DateTimeFormatter.ofPattern(format).format(data)
        }

        fun dataFormatada(data: LocalDate, format: String? = "dd/MM/yyyy"): String {
            return DateTimeFormatter.ofPattern(format).format(data)
        }

        fun verificaSessaoAberta(dtVerificacao: LocalDateTime): Boolean {
            return if (dtVerificacao != null) {
                dtVerificacao?.isAfter(LocalDateTime.now())
            } else {
                false
            }
        }

        fun unformatCpf(cpf: String?): String? {
            var cpf = cpf ?: return ""
            cpf = cpf.replace("[\\.\\-_/ ]".toRegex(), "")
            return cpf
        }

        fun isValidCpf(cpf: String?): Boolean {
            var d1: Int
            var d2: Int
            var digito1: Int
            var digito2: Int
            var resto: Int
            var digitoCPF: Int
            val nDigResult: String
            if (cpf == null || cpf.trim { it <= ' ' }.isEmpty() || java.lang.Long.valueOf(cpf.trim { it <= ' ' }) == java.lang.Long.valueOf("0") || cpf.trim { it <= ' ' } == "00000000000" || cpf.trim { it <= ' ' } == "11111111111" || cpf.trim { it <= ' ' } == "22222222222" || cpf.trim { it <= ' ' } == "33333333333" || cpf.trim { it <= ' ' } == "44444444444" || cpf.trim { it <= ' ' } == "55555555555" || cpf.trim { it <= ' ' } == "66666666666" || cpf.trim { it <= ' ' } == "77777777777" || cpf.trim { it <= ' ' } == "88888888888" || cpf.trim { it <= ' ' } == "99999999999") return false
            return if (cpf.trim { it <= ' ' }.length != 11) {
                false
            } else {
                d2 = 0
                d1 = d2
                resto = 0
                digito2 = resto
                digito1 = digito2
                for (nCount in 1 until cpf.length - 1) {
                    digitoCPF = Integer.valueOf(cpf.substring(nCount - 1, nCount)).toInt()
                    //multiplique a ultima casa por 2 a seguinte por 3 a seguinte por 4 e assim por diante.
                    d1 = d1 + (11 - nCount) * digitoCPF
                    //para o segundo digito repita o procedimento incluindo o primeiro digito calculado no passo anterior.
                    d2 = d2 + (12 - nCount) * digitoCPF
                }

                //Primeiro resto da divisão por 11.
                resto = d1 % 11
                digito1 = if (resto < 2) 0 else 11 - resto
                d2 += 2 * digito1

                //Segundo resto da divisão por 11.
                resto = d2 % 11
                digito2 = if (resto < 2) 0 else 11 - resto

                //Digito verificador do CPF que está sendo validado.
                val nDigVerific = cpf.substring(cpf.length - 2, cpf.length)
                //Concatenando o primeiro resto com o segundo.
                nDigResult = digito1.toString() + digito2.toString()
                //comparar o digito verificador do cpf com o primeiro resto + o segundo resto.
                nDigVerific == nDigResult
            }
        }
    }
}