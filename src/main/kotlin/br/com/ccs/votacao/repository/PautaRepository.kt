package br.com.ccs.votacao.repository

import br.com.ccs.votacao.model.models.Pauta
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PautaRepository : JpaRepository<Pauta, Long>