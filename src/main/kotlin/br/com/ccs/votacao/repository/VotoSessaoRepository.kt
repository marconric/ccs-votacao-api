package br.com.ccs.votacao.repository

import br.com.ccs.votacao.model.models.VotoSessao
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.stereotype.Repository

@Repository
@EnableJpaRepositories
interface VotoSessaoRepository : JpaRepository<VotoSessao, Long> {

    fun findAllBySessaoId(sessaoId: Long): List<VotoSessao>
}