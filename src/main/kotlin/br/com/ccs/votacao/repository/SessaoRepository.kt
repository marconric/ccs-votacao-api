package br.com.ccs.votacao.repository

import br.com.ccs.votacao.model.enum.SituacaoSessaoEnum
import br.com.ccs.votacao.model.models.Sessao
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
@EnableJpaRepositories
interface SessaoRepository : JpaRepository<Sessao, Long> {

    fun findFirstByPautaId(pautaId: Long): Optional<Sessao>

    fun findAllByStSessaoOrderByDtFimSessaoDesc(stSessao: SituacaoSessaoEnum): List<Sessao>

    fun findFirstByIdAndStSessao(id: Long, stSessao: SituacaoSessaoEnum): Optional<Sessao>

    fun existsSessaoByPautaId(pautaId: Long): Boolean


    @Query(value = "SELECT s FROM Sessao s LEFT JOIN FETCH s.votos where s.id = :id  ")
    override fun findById(@Param("id") id: Long): Optional<Sessao>


}