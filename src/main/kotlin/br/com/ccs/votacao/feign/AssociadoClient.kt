package br.com.ccs.votacao.feign

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.*

@FeignClient(url = "\${ccs.consultaCpf.url}", value = "consulta-cpf")
interface AssociadoClient {

    @GetMapping("/users/{cpf}")
    fun consultaAssociadoCpf(@PathVariable cpf: String): retornoConsulta

    data class retornoConsulta(
            val status: String? = ""
    )
}
