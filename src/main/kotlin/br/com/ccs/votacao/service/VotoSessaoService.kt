package br.com.ccs.votacao.service

import br.com.ccs.votacao.feign.AssociadoClient
import br.com.ccs.votacao.model.enum.SimNaoEnum
import br.com.ccs.votacao.model.enum.SituacaoSessaoEnum
import br.com.ccs.votacao.model.models.VotoSessao
import br.com.ccs.votacao.repository.SessaoRepository
import br.com.ccs.votacao.repository.VotoSessaoRepository
import br.com.ccs.votacao.utils.Utils
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.LocalDateTime


@Service
class VotoSessaoService(private val associadoClient: AssociadoClient,
                        private val sessaoRepository: SessaoRepository,
                        private val votoSessaoRepository: VotoSessaoRepository) {

    private var log = LoggerFactory.getLogger(this::class.java)

    fun realizarVoto(idSessao: Long, cpfRecebido: String, voto: SimNaoEnum): Any? {

        return try {
            log.info("Início verificação CPF ${cpfRecebido}}")
            val cpf = Utils.unformatCpf(cpfRecebido)
            if (!Utils.isValidCpf(cpf)) {
                "Problema CPF ${cpf} inválido"
            } else {


                var associadoOK = cpf?.let { associadoClient.consultaAssociadoCpf(it) }
                if (associadoOK?.status.equals("ABLE_TO_VOTE")) {
                    log.info("Computar voto associado com CPF ${cpf}}")

                    val sessaoOptional = sessaoRepository.findFirstByIdAndStSessao(idSessao, SituacaoSessaoEnum.INICIADA)
                    if (sessaoOptional.isPresent) {
                        val sessao = sessaoOptional.get()
                        if (sessao.dtFimSessao?.let { Utils.verificaSessaoAberta(it) }!!) {
                            val verificaSejaVotou = sessao.votos?.filter { it -> it?.cpf.equals(cpf) }
                            if (!verificaSejaVotou.isNullOrEmpty()) {
                                "Associado já realizou seu voto nesta Pauta"
                            } else {
                                cpf?.let { VotoSessao(null, idSessao, sessao, it, voto, LocalDateTime.now()) }?.let { votoSessaoRepository.save(it) }
                            }
                        } else {
                            "Sessão Encerrada"
                        }
                    } else {
                        "Sessão $idSessao não localizada."
                    }
                } else {
                    "Associado com CPF (${cpf}) não habilitado para votar."
                }
            }

        } catch (ex: Exception) {
            "CPF (${cpfRecebido}) é inválido."
        }
    }

}