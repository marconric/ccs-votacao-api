package br.com.ccs.votacao.service

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.Producer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.HashMap

@Service
class MensageriaKafkaService {

    @Value("\${ccs.order.topic}")
    private val topico: String? = null

    @Value(value = "\${spring.kafka.producer.bootstrap-servers}")
    private val bootstrapAddress: String? = null

    fun createConsumer(): Producer<String, String> {

        val configProps: MutableMap<String, Any> = HashMap()
        configProps[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = bootstrapAddress as Any
        configProps[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
        configProps[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java

        return KafkaProducer(configProps)
    }

    fun enviaMsg(mensagem: String): Any? {
        return if (topico != null && bootstrapAddress != null) {
            try {
                val producer = createConsumer()

                val future = producer.send(ProducerRecord("$topico", "1", mensagem))
                future.get()
            } catch (ex: Exception) {
                "Erro ao Comunicar com serviço Kafka"
            } finally {
                "Segue processo sem envio de mensagem para topico ${topico}"
            }
        } else {
            "Mensageria Kafka não está configurada verifique (application.yml)"
        }
    }
}





