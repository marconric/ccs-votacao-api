package br.com.ccs.votacao.service

import br.com.ccs.votacao.model.enum.SimNaoEnum
import br.com.ccs.votacao.model.enum.SituacaoSessaoEnum
import br.com.ccs.votacao.model.models.Pauta
import br.com.ccs.votacao.model.models.Sessao
import br.com.ccs.votacao.model.response.SessaoResponse
import br.com.ccs.votacao.repository.PautaRepository
import br.com.ccs.votacao.repository.SessaoRepository
import br.com.ccs.votacao.repository.VotoSessaoRepository
import br.com.ccs.votacao.utils.Utils
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class SessaoService(
        private val sessaoRepository: SessaoRepository,
        private val pautaRepository: PautaRepository,
        private val mensageriaKafkaService: MensageriaKafkaService,
        private val votoSessaoRepository: VotoSessaoRepository
) {

    private var log = LoggerFactory.getLogger(this::class.java)

    fun iniciarSessao(pautaId: Long, tempoSessao: Int): Any? {

        val pautaOptional = pautaRepository.findById(pautaId)
        if (!pautaOptional.isPresent) {
            return "Pauta $pautaId não localizada"
        }

        val sessaoOptional = sessaoRepository.findFirstByPautaId(pautaId)
        if (sessaoOptional.isPresent) {
            return "Sessão com a pauta $pautaId já existe com situação  ${sessaoOptional.get().stSessao.descricao}"
        }


        val dtFimSessao = LocalDateTime.now().plusMinutes(tempoSessao.toLong())

        val sessao = sessaoRepository.save(
                Sessao(
                        null, pautaId, pautaOptional.get(), tempoSessao,
                        LocalDateTime.now(), dtFimSessao, SituacaoSessaoEnum.INICIADA
                )
        )

        return sessao.pauta.ordem?.let { SessaoResponse(it, sessao.dtInicioSessao, sessao.dtFimSessao) }
    }


    fun buscaSessaoById(id: Long): Any? {
        val sessao = sessaoRepository.findById(id)
        return if (sessao.isPresent) {
            sessao.get()

        } else {
            "Sessao $id não encontrada"
        }
    }


    fun prorrogaSessao(idSessao: Long, tempoProrrogacao: Int) {
        val sessaoOptional = sessaoRepository.findFirstByIdAndStSessao(idSessao, SituacaoSessaoEnum.INICIADA)
        if (sessaoOptional.isPresent) {
            val sessao = sessaoOptional.get()
            if (sessao.dtFimSessao?.let { Utils.verificaSessaoAberta(it) }!!) {
                sessao.dtFimSessao = sessao.dtFimSessao!!.plusMinutes(tempoProrrogacao.toLong())
                sessaoRepository.save(sessao)
            } else {
                "Sessão Encerrada"
            }
        } else {
            "Sessão $idSessao não localizada."
        }
    }


    @Scheduled(cron = "\${ccs.scheduler}")
    fun verifica() {

        log.info("verificaSessao", "inicio", Utils.dataFormatada(LocalDateTime.now()))

        val listSecoes = sessaoRepository.findAllByStSessaoOrderByDtFimSessaoDesc(SituacaoSessaoEnum.INICIADA)
        if (listSecoes.isNullOrEmpty()) {
            log.info("Nenhuma sessão em andamento")
        } else {
            /*
            *  Verificando se possui alguma sessão para realizar o encerramento e computação dos votos
             */
            val list = listSecoes.filter { it -> it.dtFimSessao?.isBefore(LocalDateTime.now()) == true }
            if (!list.isNullOrEmpty()) {
                /// Encerra seção e computar votos
                log.info("Ten que encerrar Sessão")

                var listEncerraSessao: ArrayList<Sessao> = arrayListOf()
                var listPauta: ArrayList<Pauta> = arrayListOf()


                list.forEach {
                    var mensagem: String = ""
                    val votos = it.id?.let { it1 -> votoSessaoRepository.findAllBySessaoId(it1) }
                    if (!votos.isNullOrEmpty()) {

                        val qtdSim = votos?.filter { votosim -> votosim?.voto != null && votosim?.voto?.equals(SimNaoEnum.SIM) }
                        val qtdNao = votos?.filter { votoNao -> votoNao?.voto != null && votoNao?.voto?.equals(SimNaoEnum.NAO) }

                        mensagem = "${it.pautaId} Favoraveis (${qtdSim?.size}) contrários ${qtdNao?.size}"
                        log.info(mensagem)

                        it.stSessao = SituacaoSessaoEnum.ENCERRADA
                        it.dtFimSessao = LocalDateTime.now()
                        listEncerraSessao.add(it)

                        var pauta = it.pauta
                        pauta.votosFavoravel += qtdSim?.size!!
                        pauta.votosContrario += qtdNao?.size!!
                        listPauta.add(pauta)


                    } else {
                        mensagem = "${it.pautaId}  não teve votos computados"
                        log.info(mensagem)
                        it.stSessao = SituacaoSessaoEnum.ENCERRADASEMVOTO
                        it.dtFimSessao = LocalDateTime.now()
                        listEncerraSessao.add(it)
                    }
                    mensageriaKafkaService.enviaMsg(mensagem)
                }
                if (!listEncerraSessao.isNullOrEmpty()) {
                    sessaoRepository.saveAll(listEncerraSessao)
                }
                if (!listPauta.isNullOrEmpty()) {
                    pautaRepository.saveAll(listPauta)
                }
            }
        }
    }
}