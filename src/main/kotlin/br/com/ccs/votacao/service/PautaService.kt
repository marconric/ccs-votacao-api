package br.com.ccs.votacao.service

import br.com.ccs.votacao.model.models.Pauta
import br.com.ccs.votacao.model.request.PautaRequest
import br.com.ccs.votacao.model.response.PautaResponse
import br.com.ccs.votacao.repository.PautaRepository
import br.com.ccs.votacao.repository.SessaoRepository
import org.springframework.stereotype.Service

@Service
class PautaService(private val pautaRepository: PautaRepository,
                   private val sessaoRepository: SessaoRepository) {


    fun insertPauta(pautaRequest: PautaRequest): Any? {
        return PautaResponse(pautaRepository.save(Pauta(null, pautaRequest.ordem, pautaRequest.detalhamento)))
    }

    fun findAllPautas(): List<Pauta> {
        return pautaRepository.findAll()
    }

    fun findPautaById(id: Long): Any? {
        val pauta = pautaRepository.findById(id)
        return if (pauta.isPresent) {
            pauta.get()

        } else {
            "Pauta $id não encontrada"
        }
    }

    fun excluirPauta(id: Long): Any? {
        return if (sessaoRepository.existsSessaoByPautaId(id)) {
            "Pauta {id} não pode ser excluida já possui uma sessão marcada"
        } else {

            val pauta = pautaRepository.findById(id)
            if (pauta.isPresent) {
                sessaoRepository.deleteById(id)
            } else {
                "Pauta $id não localizada"
            }

        }
    }
}