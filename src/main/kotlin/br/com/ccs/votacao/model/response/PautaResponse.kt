package br.com.ccs.votacao.model.response

import br.com.ccs.votacao.model.models.Pauta

class PautaResponse {

    var id: Long? = 0L
    var ordem: String? = ""
    var detalhamento: String? = ""
    var sim: Int = 0
    var nao: Int = 0
    var resultado: String = ""

    constructor(pauta: Pauta) {
        this.id = pauta.id
        this.ordem = pauta.ordem
        this.detalhamento = pauta.detalhamento
        this.resultado = getResultadoPauta(pauta.votosFavoravel, pauta.votosContrario)
    }

    fun getResultadoPauta(sim: Int, nao: Int): String {
        return when {
            sim > nao -> "Pauta Aprovada"
            sim == nao -> "Pauta Empatada"
            sim == 0 && nao == 0 -> "Sem Votos Computados"
            else -> "Pauta Reprovada"
        }
    }

}