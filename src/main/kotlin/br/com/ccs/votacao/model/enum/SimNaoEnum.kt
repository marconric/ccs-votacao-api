package br.com.ccs.votacao.model.enum

enum class SimNaoEnum(val codigo: String, val descricao: String) {
    SIM("S", "Voto favorável a Pauta"),
    NAO("N", "Voto Contrário a Pauta");

    companion object {
        fun getById(id: String): SimNaoEnum? = values().firstOrNull { it.codigo == id }
    }
}