package br.com.ccs.votacao.model.request

data class PautaRequest(
        val ordem: String? = "",
        val detalhamento: String? = ""
)