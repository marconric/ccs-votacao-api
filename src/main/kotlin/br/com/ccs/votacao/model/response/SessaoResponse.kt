package br.com.ccs.votacao.model.response

import br.com.ccs.votacao.utils.Utils
import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDateTime

data class SessaoResponse(

        var ordem: String,
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        var dataInicio: LocalDateTime? = null,
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        var dataFim: LocalDateTime? = null
) {
    fun getDataInicioFmt(): String {
        return if (this.dataInicio != null) {
            Utils.dataFormatada(this.dataInicio!!, "dd/MM/yyyy HH:mm:ss")
        } else {
            ""
        }
    }

    fun getDataFimFmt(): String {
        return if (this.dataFim != null) {
            Utils.dataFormatada(this.dataFim!!, "dd/MM/yyyy HH:mm:ss")
        } else {
            ""
        }
    }
}