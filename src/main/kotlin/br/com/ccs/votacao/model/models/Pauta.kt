package br.com.ccs.votacao.model.models

import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import java.io.Serializable
import javax.persistence.*

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = Pauta.Companion.CONFIG.TABLE)
data class Pauta(

        @Id
        @SequenceGenerator(name = CONFIG.SEQUENCE, sequenceName = CONFIG.SEQUENCE, allocationSize = 1)
        @GeneratedValue(strategy = GenerationType.AUTO, generator = CONFIG.SEQUENCE)
        @Column(name = "ID")
        var id: Long? = null,

        @Column(name = "ORDEM")
        val ordem: String? = null,

        @Column(name = "DETALHAMENTO")
        val detalhamento: String? = null,

        @Column(name = "VOTOS_FAVORAVEL")
        var votosFavoravel: Int = 0,

        @Column(name = "VOTOS_CONTRARIO")
        var votosContrario: Int = 0

) : Serializable {
    companion object {
        object CONFIG {
            const val TABLE = "PAUTA"
            const val SEQUENCE = "${TABLE}_SEQ_ID"
        }
    }
}