package br.com.ccs.votacao.model.enum

enum class SituacaoSessaoEnum(val codigo: String, val descricao: String) {
    INICIADA("I", "Sessão Iniciada"),
    ENCERRADASEMVOTO("S", "Sessão encerrada Sem Voto"),
    ENCERRADA("E", "Sessão Encerrada");

    companion object {
        fun getById(id: String): SituacaoSessaoEnum? = values().firstOrNull { it.codigo == id }
    }
}