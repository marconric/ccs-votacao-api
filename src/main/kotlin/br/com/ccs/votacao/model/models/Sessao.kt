package br.com.ccs.votacao.model.models

import br.com.ccs.votacao.model.enum.SituacaoSessaoEnum
import com.fasterxml.jackson.annotation.JsonFormat
import org.hibernate.annotations.*
import java.io.Serializable
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = Sessao.Companion.CONFIG.TABLE)
data class Sessao(

        @Id
        @SequenceGenerator(name = CONFIG.SEQUENCE, sequenceName = CONFIG.SEQUENCE, allocationSize = 1)
        @GeneratedValue(strategy = GenerationType.AUTO, generator = CONFIG.SEQUENCE)
        @Column(name = "ID")
        var id: Long? = null,

        @Column(name = "PAUTA_ID")
        val pautaId: Long? = null,

        @OneToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "PAUTA_ID", referencedColumnName = "ID", insertable = false, updatable = false)
        val pauta: Pauta,

        @Column(name = "TEMPO")
        val tempo: Int = 60,

        @Column(name = "DT_INICIO_SESSAO")
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        val dtInicioSessao: LocalDateTime? = null,

        @Column(name = "DT_FIM_SESSAO")
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        var dtFimSessao: LocalDateTime? = null,

        @Column(name = "ST_SESSAO")
        var stSessao: SituacaoSessaoEnum = SituacaoSessaoEnum.INICIADA,

        @OneToMany(mappedBy = "sessao", cascade = [CascadeType.PERSIST, CascadeType.REMOVE], fetch = FetchType.LAZY)
        @Fetch(value = FetchMode.JOIN)
        var votos: List<VotoSessao?>? = arrayListOf()

) : Serializable {
    companion object {
        object CONFIG {
            const val TABLE = "SESSAO"
            const val SEQUENCE = "${TABLE}_SEQ_ID"
        }
    }
}