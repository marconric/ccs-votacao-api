package br.com.ccs.votacao.model.models

import br.com.ccs.votacao.model.enum.SimNaoEnum
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = VotoSessao.Companion.CONFIG.TABLE)
data class VotoSessao(

        @Id
        @SequenceGenerator(name = CONFIG.SEQUENCE, sequenceName = CONFIG.SEQUENCE, allocationSize = 1)
        @GeneratedValue(strategy = GenerationType.AUTO, generator = CONFIG.SEQUENCE)
        @Column(name = "ID")
        var id: Long? = null,

        @Column(name = "SESSAO_ID")
        val sessaoId: Long? = null,

        @JsonIgnore
        @javax.persistence.ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "SESSAO_ID", referencedColumnName = "ID", insertable = false, updatable = false)
        var sessao: Sessao? = null,

        @Column(name = "CPF")
        val cpf: String = "",

        @Column(name = "VOTO")
        val voto: SimNaoEnum,

        @Column(name = "DT_VOTACAO")
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        val dtVotacao: LocalDateTime? = null

) : Serializable {
    companion object {
        object CONFIG {
            const val TABLE = "VOTACAO"
            const val SEQUENCE = "${TABLE}_SEQ_ID"
        }
    }
}